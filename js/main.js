$(window).resize(function() {
    if ($(window).width() < 640) {
       $(".menumobile__button").show();
    }
   else {
        $(".menumobile__button").hide();
   }
});

$(".fa-bars").click(function () {
    $(this).css('display', 'none');
    $(".fa-times").css('display', 'block');
    $(".offscreen__menu").fadeIn(1500);
    $(".offscreen__menu").css({
        'display': 'flex',
        'justify-content' : 'center',
        'align-items' : 'center'
    });

});

$(".fa-times").click(function () {
    $(this).css('display', 'none');
    $(".fa-bars").css('display', 'block');
    $(".offscreen__menu").fadeOut(1500);
})