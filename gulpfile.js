/*
Olá pessoal da One Digital, tudo bem?
Eu usei mais o NPM porque eu acredito que há alguma complicações em usar o gulp
e tenho levado isso em conta nos meus ambientes de desenvolvimento.
Esse link (https://medium.freecodecamp.org/why-i-left-gulp-and-grunt-for-npm-scripts-3d6853dd22b8)
tem algumas das explicações mas, de qualquer forma fiz o browser sync para mostrar que, se
necessário for eu também sei utilizar o gulp.
Obrigado
*/


var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;


gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("*.html").on("change", reload);
});


